FROM node:18.13.0-slim as build
WORKDIR /usr/local/app

COPY ./alerts/package.json .
COPY ./alerts/yarn.lock .
RUN npm i -g @angular/cli@15.1.3
RUN yarn install

COPY ./alerts/ .

RUN ng build

FROM nginx:1.23.3-alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/local/app/dist/alerts /usr/share/nginx/html
EXPOSE 80

