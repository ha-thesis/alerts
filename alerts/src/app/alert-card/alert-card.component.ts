import { Component, Input } from '@angular/core';
import { Alert } from '../models/Alert';

@Component({
  selector: 'app-alert-card',
  templateUrl: './alert-card.component.html',
  styleUrls: ['./alert-card.component.scss']
})
export class AlertCardComponent {
  @Input() alert!: Alert;
  timeAgo: string = "Now!";

  ngOnInit(): void {
    this.updateTimeAgo();
    setInterval(() => {
      this.updateTimeAgo();
    }, 1000);
  }

  updateTimeAgo(): void {
    this.timeAgo = this.getTimeAgo();
  }

  getTimeAgo(): string {
    const timeAgo = new Date().getTime() - new Date(this.alert.timeStarted).getTime();
    const seconds = Math.floor(timeAgo / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30);
    const years = Math.floor(months / 12);
    if (years > 0) {
      return years + ' years ago';
    } else if (months > 0) {
      return months + ' months ago';
    } else if (days > 0) {
      return days + ' days ago';
    } else if (hours > 0) {
      return hours + ' hours ago';
    } else if (minutes > 0) {
      return minutes + ' minutes ago';
    } else {
      return seconds + ' seconds ago';
    }
  }


}
