import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertCardComponent } from './alert-card/alert-card.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AlertPageComponent } from './pages/alert-page/alert-page.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertCardComponent,
    NavbarComponent,
    AlertPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
