export class Alert {
    id: number;
    deviceId: string;
    averagePulse: number;
    currentPulse: number;
    timeStarted: Date;

    constructor(id: number, deviceId: string, averagePulse: number, currentPulse: number, timeStarted: Date) {
        this.id = id;
        this.deviceId = deviceId;
        this.averagePulse = averagePulse;
        this.currentPulse = currentPulse;
        this.timeStarted = timeStarted;
    }
}
