import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/internal/operators/filter';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isAlerts: boolean = false;
  path: string = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.getPath();
    this.setActiveButton();
  }

  setActiveButton(): void {
    this.reset();
    if (this.path === "/alerts") {
      this.isAlerts = true;
    }
  }

  reset(): void {
    this.isAlerts = false;
  }

  getPath(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(event => {
      this.path = event["urlAfterRedirects"];
      this.setActiveButton();
    });
  }

}
