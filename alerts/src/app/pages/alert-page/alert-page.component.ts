import { Component } from '@angular/core';
import { Alert } from 'src/app/models/Alert';
import { AlertsService } from 'src/app/services/alerts.service';

@Component({
  selector: 'app-alert-page',
  templateUrl: './alert-page.component.html',
  styleUrls: ['./alert-page.component.scss']
})
export class AlertPageComponent {
  alerts: Alert[] = [];

  constructor(private alersService: AlertsService) { }

  ngOnInit(): void {
    this.updateAlerts();
  }

  updateAlerts = () => {
    this.alersService.getAlerts().subscribe(alerts => {
      const newAlerts: Alert[] = [];
      alerts.forEach(alert => {
        newAlerts.push(new Alert(
          alert.id,
          alert.deviceId,
          alert.averagePulse,
          alert.currentPulse,
          new Date(alert.timeStarted)
        ));
      });
      newAlerts.sort((a, b) => {
        return b.timeStarted.getTime() - a.timeStarted.getTime();
      });
      this.alerts = newAlerts;
    })
  };

}
