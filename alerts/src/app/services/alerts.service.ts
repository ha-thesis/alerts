import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Alert } from '../models/Alert';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  apiAddress = "https://alerts-api.c1.papug.cloud";

  constructor(private http: HttpClient) { }

  getAlerts(): Observable<Alert[]> {
    return this.http.get<Alert[]>(this.apiAddress + '/alerts');
  }
}
